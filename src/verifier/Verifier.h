/*
 * Verifier.h
 *
 *  Created on: 01.04.2019
 *  Author: sefi16
 */

#ifndef VERIFIER_H
#define VERIFIER_H

#include <list>
#include "../board/Board.h"
#include "../board/Chessboard.h"
#include "../utils/PossibleMovesFromField.h"

class Verifier {

	private:

	public:
		Verifier();
		~Verifier();
		bool verify(Board& board, std::list<PossibleMovesFromField>& lst);

};



#endif /* VERIFIER_H */
