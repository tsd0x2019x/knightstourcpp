/*
 * Verifier.cpp
 *
 *  Created on: 01.04.2019
 *  Author: sefi16
 */
#include "Verifier.h"

Verifier::Verifier() {}

Verifier::~Verifier() {}

/**
 *
 */

//bool Verifier::verify(Board& board, std::list<PossibleMovesFromField>& lst) {
//	bool bResult = true;
//	int iListSize = lst.size();
//	if (iListSize != (board.getHeight()*board.getWidth())) {
//		bResult = false;
//	}
//	else {
//		Board * newBoard = new Board(board.getHeight(), board.getWidth());
//		for(std::list<PossibleMovesFromField>::iterator ite = lst.begin(); ite != lst.end(); ite++) {
//			Field f = ite->getCurrentField();
//			if (!newBoard->isEmptyField(f.row,f.col)) {
//				bResult = false;
//				break;
//			}
//			else {
//				newBoard->setField(f.row,f.col);
//			}
//		}
//		delete newBoard;
//	}
//	return bResult;
//}

bool Verifier::verify(Board& board, std::list<PossibleMovesFromField>& lst) {
	if (((int)lst.size()) != (board.getHeight()*board.getWidth())) {
		return false;
	}
	else {
		// Workaround: Avoid error "free(): invalid pointer"
		Board * newBoard = new Board(board.getHeight(),board.getWidth());
		bool bResult = true;
		for(std::list<PossibleMovesFromField>::iterator ite = lst.begin(); ite != lst.end(); ite++) {
			Field f = ite->getCurrentField();
			if (!newBoard->isEmptyField(f.row,f.col)) {
				bResult = false;
				break;
			}
			else {
				newBoard->setField(f.row,f.col);
			}
		}
		delete newBoard;
		return bResult;
	}
}



