/*
 * Field.h
 *
 *  Created on: 25.03.2019
 *  Author: sefi16
 */

#ifndef FIELD_H
#define FIELD_H

struct Field { // All attributes and functions in <struct> are automatically public

	int row, col;

	Field(int iRow, int iCol) {
		row = iRow;
		col = iCol;
	}

};

#endif /* FIELD_H */
