/*
 * PossibleMovesFromField.h
 *
 *  Created on: 25.03.2019
 *  Author: sefi16
 */

#ifndef POSSIBLEMOVESFROMFIELD_H
#define POSSIBLEMOVESFROMFIELD_H

#include "Field.h"
#include <list>

class PossibleMovesFromField {

	private:
		Field m_currentField;
		std::list<Field> m_possibleMoves;
		std::list<Field>::iterator m_ite;

	public:
		PossibleMovesFromField(Field currentField);
		~PossibleMovesFromField();
		void addElement(Field f);
		void addElement(int iRow, int iCol);
		Field& getCurrentField();
		Field getElement(int index);
		int getSize();
		Field pop_back();
		Field removeElement(int index);
		std::list<Field>::iterator& getIterator(); // return the current (position of) iterator <m_ite>
		void setIteratorToListBegin();

};


#endif /* POSSIBLEMOVESFROMFIELD_H */
