/*
 * PossibleMovesFromField.cpp
 *
 *  Created on: 25.03.2019
 *  Author: sefi16
 */
#include <iostream>
#include "PossibleMovesFromField.h"

PossibleMovesFromField::PossibleMovesFromField(Field currentField) : m_currentField(currentField), m_possibleMoves() {
	m_ite = m_possibleMoves.begin();
}

PossibleMovesFromField::~PossibleMovesFromField() {
	m_possibleMoves.clear();
}

/**
 * Appends the specified field (element) at the end of this list.
 * This list contains the next possible moves of the knight (Springer) from the specified field.
 * After adding new element at list's back, the iterator (pointer) points to the last element.
 */
void PossibleMovesFromField::addElement(Field f) {
	m_possibleMoves.push_back(f);
}

/**
 * Appends the specified field (element) with <iRow> and <iCol> at the end of this list.
 * This list contains the next possible moves of the knight (Springer) from the specified field.
 * @param iRow : Row index of field to start from
 * @param iCol : Column index of field to start from
 */
void PossibleMovesFromField::addElement(int iRow, int iCol) {
	Field f = Field(iRow, iCol);
	m_possibleMoves.push_back(f);
}

/**
 * Provides the (reference of the) current field where the knight (Springer) is currently in.
 */
Field& PossibleMovesFromField::getCurrentField() {
	return m_currentField;
}

/**
 * Provides the (reference of) element with specified <index> of this list.
 * If element with index <index> cannot be found, the first element will be returned instead.
 * @param index : Index of the field to be provided
 */
Field PossibleMovesFromField::getElement(int index) {
	int i = 0;
	for(m_ite = m_possibleMoves.begin(); m_ite != m_possibleMoves.end(); m_ite++) {
		if (i == index) {
			return (*m_ite);
		}
		i += 1;
	}
	return (*m_possibleMoves.begin());
}

/**
 * Return the size of this list (i.e. number of next possible moves, starting from <m_currenField>)
 * @return Size of this list (i.e. number of next possible moves, starting from <m_currenField>)
 */
int PossibleMovesFromField::getSize() {
	return m_possibleMoves.size();
}

/**
 * Pop the last element of this list (i.e. removes the last element and provide it).
 * If the list is empty (i.e. list.size() == 0), then <i>NULL</i> will be returned.
 * @return The last element that was removed from the list previously. If the list is empty, <i>NULL</i> is returned.
 */
Field PossibleMovesFromField::pop_back() {
	Field f = m_possibleMoves.back();
	m_possibleMoves.pop_back();
	return f;
}

/**
 * Removes the element at <index> from this list and returns this element. The <index> is in range [0,size()-1].
 * After removing, the iterator internally points to the following/next position/element in the list.
 * @return Field that is previously removed from this list.
 */
Field PossibleMovesFromField::removeElement(int index) {
	m_ite = m_possibleMoves.begin();
	int i = 0;
	while (i < index) {
		m_ite++;
		i += 1;
	}
	Field f = *m_ite; // Make copy
	m_ite = m_possibleMoves.erase(m_ite); // Remove element at iterator position from list.
	// After removing, the iterator internally points to the following/next position/element in the list.
	return f; // Return copy of the removed element.
}

/**
 * Return the current (position of) iterator <m_ite>. Iterator is already a pointer itself.
 */
std::list<Field>::iterator& PossibleMovesFromField::getIterator() {
	return m_ite;
}

/**
 * Set the iterator to first element (begin) of list.
 */
void PossibleMovesFromField::setIteratorToListBegin() {
	m_ite = m_possibleMoves.begin();
}
