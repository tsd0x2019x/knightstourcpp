/*
 * KnightsTour.h
 *
 * This program demonstrates and solves the Knight's tour problem (Springerproblem),
 * using backtracking (brute force) with/without Warndorf's rule.
 *
 *  Created on: 27.03.2019
 *  Author: sefi16
 */

#ifndef KNIGHTSTOUR_H
#define KNIGHTSTOUR_H

#include <list>
#include "../board/Chessboard.h"
#include "../utils/PossibleMovesFromField.h"

#define M_DEBUG true

class KnightsTour {

	private:
		Chessboard m_board;
		std::list<PossibleMovesFromField> m_movesList; // Create an empty list.
		bool isValidField(int iRow, int iCol);
		PossibleMovesFromField pop_back();
		int getNumberOfNextMoves(int iRow, int iCol);
		void searchPossibleNextMovesFromField(int iRow, int iCol, PossibleMovesFromField * pm);

	public:
		KnightsTour(Chessboard& board);
		~KnightsTour();
		bool backtracking_Warndorf();
		bool findPath_Warndorf(int iRow, int iCol);
		void clearAll();
		void resetAll();
		std::list<PossibleMovesFromField> getList();
		void printPath();

};

#endif /* KNIGHTSTOUR_H */
