/*
 * KnightsTour.cpp
 *
 *  Created on: 27.03.2019
 *  Author: sefi16
 */
#include <iostream>
#include <string>
#include "KnightsTour.h"

KnightsTour::KnightsTour(Chessboard& board) : m_board(board), m_movesList() {
//	m_board = board;
//	m_movesList = std::list<PossibleMovesFromField>();
	m_board.resetBoard();
}

KnightsTour::~KnightsTour() {}

/**
 * Check if the specified field with <i>iRow</i> and <i>iCol</i> is valid (i.e. free and within board).
 * @param iRow : Row index of the specified field to be checked.
 * @param iCol : Column index of the specified field to be checked.
 * @return <i>true</i> : The specified field is valid (i.e. free and within board)<br /><i>false</i> : Otherwise
 */
bool KnightsTour::isValidField(int iRow, int iCol) {
	return m_board.isValidField(iRow,iCol);
}

/**
 * Pop the last element of list <i>m_movesList</i> (i.e. remove the last element from list and provide it).
 * @return The last element (from list <i>m_movesList</i>) that was removed previously.
 */
PossibleMovesFromField KnightsTour::pop_back() {
	PossibleMovesFromField pm = m_movesList.back();
	m_movesList.pop_back();
	return pm;
}

/**
 * Provides number of all next possible moves starting from the specified field with <i>iRow</i> and <i>iCol</i>.
 * @param iRow : Row index of field to start from
 * @param iCol : Column index of field to start from
 * @return Number of all next possible moves starting from the specified field with <i>iRow</i> and <i>iCol</i>.
 */
int KnightsTour::getNumberOfNextMoves(int iRow, int iCol) {
	int iNumber = 0;

	// 1st possible next move
	if (isValidField(iRow-2, iCol-1)) iNumber++;
	// 2nd possible next move
	if (isValidField(iRow-1, iCol-2)) iNumber++;
	// 3rd possible next move
	if (isValidField(iRow+1, iCol-2)) iNumber++;
	// 4th possible next move
	if (isValidField(iRow+2, iCol-1)) iNumber++;
	// 5th possible next move
	if (isValidField(iRow+2, iCol+1)) iNumber++;
	// 6th possible next move
	if (isValidField(iRow+1, iCol+2)) iNumber++;
	// 7th possible next move
	if (isValidField(iRow-1, iCol+2)) iNumber++;
	// 8th possible next move
	if (isValidField(iRow-2, iCol+1)) iNumber++;

	return iNumber;
}

/**
 * Find the next possibles/valid fields to move to, from the current field (iRow,iCol).
 * The found possible/valid fields are going to be stored in a linked list of class <PossibleMovesFromField>,
 * which also includes the current field.
 * @param iRow : Source row index of current field, where to start from.
 * @param iCol : Source column index of current field, where to start from.
 * @param pm : Reference of the linked list, where to store the found next possible/valid fields in.
 */
void KnightsTour::searchPossibleNextMovesFromField(int iRow, int iCol, PossibleMovesFromField * pm) {
	// 1st possible next move
	if (isValidField(iRow-2, iCol-1)) pm->addElement(iRow-2, iCol-1);
	// 2nd possible next move
	if (isValidField(iRow-1, iCol-2)) pm->addElement(iRow-1, iCol-2);
	// 3rd possible next move
	if (isValidField(iRow+1, iCol-2)) pm->addElement(iRow+1, iCol-2);
	// 4th possible next move
	if (isValidField(iRow+2, iCol-1)) pm->addElement(iRow+2, iCol-1);
	// 5th possible next move
	if (isValidField(iRow+2, iCol+1)) pm->addElement(iRow+2, iCol+1);
	// 6th possible next move
	if (isValidField(iRow+1, iCol+2)) pm->addElement(iRow+1, iCol+2);
	// 7th possible next move
	if (isValidField(iRow-1, iCol+2)) pm->addElement(iRow-1, iCol+2);
	// 8th possible next move
	if (isValidField(iRow-2, iCol+1)) pm->addElement(iRow-2, iCol+1);
}


/**
 * Back tracking with Warndorf's rule
 */
bool KnightsTour::backtracking_Warndorf() {
	if (m_movesList.size() > 0) {
		PossibleMovesFromField pm = pop_back(); // Remove and return the last element from list <m_movesList>
		int iSize = pm.getSize();
		if (iSize == 0) {
			Field current = pm.getCurrentField();
			m_board.unsetField(current.row,current.col); // Mark the field as free again
			if (M_DEBUG) m_board.printBoard();
			return backtracking_Warndorf();
		}
		// pm.getSize() == iSize > 0
		int iIndex = 0; // Warndorf's rule: Index of the next field from which the number of next possible moves is at least.
		int iNum = 9; // Warndorf's rule: The number of next possible moves from field with index <iIndex>.
		int i = 0; // counter 0..7, since there are at most 8 next possible moves from (each) current field.
		while (i < iSize) {
			Field f = pm.getElement(i); // Get an element (field) from list in <pm>
			int iTmpNum = this->getNumberOfNextMoves(f.row,f.col);
			if (iTmpNum != 0 && iTmpNum < iNum) {
				iNum = iTmpNum; // Get the minimum
				iIndex = i;
			}
			++i;
		}
		Field f = pm.removeElement(iIndex); // Get and remove the specified element from list.
		m_movesList.push_back(pm); // Insert new element <pm> at list end.
		return findPath_Warndorf(f.row, f.col);
	}
	else { // m_movesList.size() == 0
		return false; // No path!
	}
}

/**
 * Find path recursively for the knight's tour problem (Springerproblem), using back-tracking (brute-force).
 * Additionally use the Warndorf's rule to optimize (decrease) the runtime.
 * @param iRow : Row index of field to start from
 * @param iCol : Column index of field to start from
 * @return <i>true</i>: A path for Knight's tour problem (Springerproblem) is found.<br/><i>false</i>: No path can be found.
 */
bool KnightsTour::findPath_Warndorf(int iRow, int iCol) {

	Field current = Field(iRow, iCol);
	PossibleMovesFromField pm = PossibleMovesFromField(current);

	m_board.setField(iRow,iCol); // Mark the field as not-free
	if (M_DEBUG) m_board.printBoard();

	// Search for the next possible/valid fields to move to, starting from current field (iRow,iCol).
	// Then, store the found possible/valid fields in linked list in <pm>.
	searchPossibleNextMovesFromField(iRow, iCol, &pm);

	if (pm.getSize() > 0) { // If there are some next possible/valid moves (fields) from current field
		int iSize = pm.getSize();
		int iIndex = 0; // Warndorf's rule: Index of the next field from which the number of next possible moves is at least.
		int iNum = 9; // Warndorf's rule: The number of next possible moves from field with index <iIndex>.
		int i = 0; // counter 0..7, since there are at most 8 next possible moves from (each) current field.
		while (i < iSize) {
			Field f = pm.getElement(i); // Get an element (field) from list in <pm>
			int iTmpNum = this->getNumberOfNextMoves(f.row,f.col);
			if (iTmpNum != 0 && iTmpNum < iNum) {
				iNum = iTmpNum;
				iIndex = i;
			}
			++i;
		}
		Field f = pm.removeElement(iIndex); // Get and remove the specified element from list.
		m_movesList.push_back(pm); // Insert new element <pm> at list end.
		return findPath_Warndorf(f.row, f.col);
	}
	else { // pm.getSize() == 0
		if (((int)m_movesList.size()) == (m_board.getHeight()*m_board.getWidth()-1)) { // Path found => Success
			m_movesList.push_back(pm);
			return true; // Path found.
		}
		else { // Dead end (Sackgasse) => Turn back (back tracking)
			m_board.unsetField(iRow,iCol);
			return backtracking_Warndorf();
		}
	}
}


/**
 * Reset (empty) the list and board. Equivalent to method <i>resetAll()</i>.
 */
void KnightsTour::clearAll() {
	m_movesList.clear();
	m_board.clearBoard();
}

/**
 * Reset (empty) the list and board. Equivalent to method <i>clearAll()</i>.
 */
void KnightsTour::resetAll() {
	clearAll();
}

/**
 * Return the whole list of moves.
 */
std::list<PossibleMovesFromField> KnightsTour::getList() {
	return m_movesList;
}

/**
 * Prints whole path of Knight's tour (Springerproblem).
 */
void KnightsTour::printPath() {
	std::string sPath ("");
	std::list<PossibleMovesFromField>::iterator ite = m_movesList.begin();
	for(unsigned int i = 0; i < (m_movesList.size()-1) && ite != m_movesList.end(); i++) {
		sPath += "(" + std::to_string(ite->getCurrentField().row) + "," + std::to_string(ite->getCurrentField().col) + ")->";
		++ite;
	}
	sPath += "("+ std::to_string(ite->getCurrentField().row) + "," + std::to_string(ite->getCurrentField().col) + ")";
	std::cout << "Path length: " << std::to_string(m_movesList.size()) << std::endl;
	std::cout << "Path: " << sPath << std::endl;
}
