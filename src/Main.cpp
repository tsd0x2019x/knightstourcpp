/*
 * Main.cpp
 *
 *  Created on: 25.03.2019
 *  Author: sefi16
 */
#include <iostream>
#include "board/Chessboard.h"
#include "utils/PossibleMovesFromField.h"
#include "solver/KnightsTour.h"
#include "verifier/Verifier.h"

int main() {

	std::cout << "The knight's tour problem (Springerproblem) solver." << std::endl;

	Chessboard MyBoard = Chessboard(8); // Standard 8x8 = 64 fields chessboard

	std::cout << "Chessboard's state before starting solver:" << std::endl;
	MyBoard.printBoard();

	KnightsTour Knight = KnightsTour(MyBoard);
	if (Knight.findPath_Warndorf(0,0)) {
		std::cout << "Result: Path found." << std::endl;
	}
	else {
		std::cout << "Result: There's no path." << std::endl;
	}

	std::cout << "Chessboard's state after solver's finished:" << std::endl;
	MyBoard.printBoard();

	Verifier Ver = Verifier();
	std::list<PossibleMovesFromField> MyMovesList = Knight.getList();

	if (Ver.verify(MyBoard, MyMovesList)) {
		std::cout << "Verification result: Correct!" << std::endl << std::endl;;
		Knight.printPath();
	}
	else {
		std::cout << "Verification result: False!" << std::endl;
	}

	return 0;
}




