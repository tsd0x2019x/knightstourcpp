/*
 * Board.cpp
 *
 *  Created on: 25.03.2019
 *  Author: sefi16
 */
#include <iostream>
#include <string>
#include "Board.h"

Board::Board(int iHeight, int iWidth) {
	initialize(iHeight, iWidth);
}

Board::~Board() {
	for(int i = 0; i < m_iHeight; i++) {
		delete[] m_arrBoard[i];
	}
	delete[] m_arrBoard;
}

void Board::initialize(int iHeight, int iWidth) {
	m_iHeight = iHeight;
	m_iWidth = iWidth;
	// Board as two-dimensional array m_arrBoard[iHeight][iWidth]
	m_arrBoard = new bool*[iHeight]; // Array of pointers
	for(int i = 0; i < iHeight; i++) {
		m_arrBoard[i] = new bool[iWidth];
	}
	// Initializes that array as empty (false)
	for(int i = 0; i < iHeight; i++) {
		for(int j = 0; j < iWidth; j++) {
			m_arrBoard[i][j] = false; // empty field
		}
	}
}

int Board::getHeight() {
	return m_iHeight;
}

int Board::getWidth() {
	return m_iWidth;
}

bool Board::isOutOfBoard(int iRow, int iCol) {
	return (iRow < 0 || iRow >= m_iHeight || iCol < 0 || iCol >= m_iWidth) ? true : false;
}

bool Board::isEmptyField(int iRow, int iCol) {
	return isOutOfBoard(iRow,iCol) ? false : (m_arrBoard[iRow][iCol]) ? false : true;
}

bool Board::isValidField(int iRow, int iCol) {
	return isEmptyField(iRow,iCol);
}

void Board::printBoard() {
	std::string s = ""; // std::string s ("");
	for(int i = 0; i < m_iHeight; i++) {
		for(int j = 0; j < m_iWidth; j++) {
			if (m_arrBoard[i][j]) { // not empty
				s += " \u25A0";
			}
			else { // empty
				s += " \u25A1";
			}
		}
		s += '\n';
	}
	std::cout << s << std::endl;
}

void Board::resetBoard() {
	for(int i = 0; i < m_iHeight; i++) {
		for(int j = 0; j < m_iWidth; j++) {
			m_arrBoard[i][j] = false; // empty field
		}
	}
}

void Board::clearBoard() {
	resetBoard();
}

void Board::setField(int iRow, int iCol) {
	m_arrBoard[iRow][iCol] = true;
}

void Board::unsetField(int iRow, int iCol) {
	m_arrBoard[iRow][iCol] = false;
}
