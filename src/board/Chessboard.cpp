/*
 * Chessboard.cpp
 *
 *  Created on: 25.03.2019
 *  Author: sefi16
 */
#include "Chessboard.h"

// Only call superclass constructor in the initialization list
Chessboard::Chessboard(int iBoardSize) : Board(iBoardSize, iBoardSize) {
	m_iBoardSize = iBoardSize;
}

// 8x8 = 64 fields
Chessboard::Chessboard() : Chessboard(8) {
	m_iBoardSize = 8;
}

Chessboard::~Chessboard() {}

int Chessboard::getSize() {
	return m_iBoardSize;
}




