/*
 * Chessboard.h
 *
 *  Created on: 25.03.2019
 *  Author: sefi16
 */

#ifndef CHESSBOARD_H
#define CHESSBOARD_H

#include "Board.h"

/**
 * Chessboard is an quadratic/sqsuare board with same value of height and width.
 */
class Chessboard : public Board {

	private:
		int m_iBoardSize;

	public:
		Chessboard(int iBoardSize);
		Chessboard(); // 8x8 = 64 fields
		~Chessboard();
		int getSize();

};



#endif /* CHESSBOARD_H */
