/*
 * Board.h
 *
 *  Created on: 25.03.2019
 *  Author: sefi16
 */

#ifndef BOARD_H
#define BOARD_H

class Board {

	private:
		// Member variables
		int m_iHeight, m_iWidth;
		bool ** m_arrBoard;
		// Methods
		void initialize(int iHeight, int iWidth);

	public:
		Board(int iHeight, int iWidth);
		~Board();
		int getHeight();
		int getWidth();
		bool isOutOfBoard(int iRow, int iCol);
		bool isEmptyField(int iRow, int iCol);
		bool isValidField(int iRow, int iCol);
		void printBoard();
		void resetBoard();
		void clearBoard();
		void setField(int iRow, int iCol);
		void unsetField(int iRow, int iCol);
};


#endif /* BOARD_H */
